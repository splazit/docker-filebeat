FROM docker.elastic.co/beats/filebeat:7.4.1
ENV FILEBEAT_CONFIG_URL=""
USER root
COPY entry.sh /entry.sh
RUN chmod +x /entry.sh
ENTRYPOINT ["/entry.sh"]
